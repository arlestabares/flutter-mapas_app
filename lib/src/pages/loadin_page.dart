import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

import 'package:mapas_app/src/helpers/helpers.dart';
import 'package:mapas_app/src/pages/acceso_gps_page.dart';
import 'package:mapas_app/src/pages/mapa_page.dart';

//import 'package:mapas_app/src/pages/mapa_page.dart';

class LoadingPage extends StatefulWidget {
  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> with WidgetsBindingObserver {
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      if (await Geolocator.isLocationServiceEnabled()) {
        Navigator.pushReplacement(
            context, navegarMapaFadeIn(context, MapaPage()));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    //
    return Scaffold(
      body: FutureBuilder(
        future: this.checkGpsYLocation(context),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          //
          print('Datos del snapshot :' + snapshot.data);
          if (snapshot.hasData) {
            return Center(child: Text(snapshot.data));
          } else {
            return Center(child: CircularProgressIndicator(strokeWidth: 3));
          }
        },
      ),
    );
  }

  Future checkGpsYLocation(BuildContext context) async {
    //Gps esta activo
    final gpsIsActivo = await Geolocator.isLocationServiceEnabled();
    //Permiso Gps
    final permisoGps = await Permission.location.isGranted;

    //
    print( 'Valor del gpsIsActivo: $gpsIsActivo');
    print('Valor del permisoGps:  $permisoGps');
    //
    if (permisoGps && gpsIsActivo) {
      Navigator.pushReplacement(
          context, navegarMapaFadeIn(context, MapaPage()));
      return '-----Entro aqui----';
      //
    } else if (!permisoGps) {
      Navigator.pushReplacement(
          context, navegarMapaFadeIn(context, AccesoGpsPage()));
      return 'Es necesario el permiso del GPS';
      //
    } else {
      return 'Active el GPS';
    }

    //await Future.delayed(Duration(milliseconds: 1000));

    //Funcion para cambiar de  pagina donde hacemos la transicion

    // Navigator.pushReplacement(context, navegarMapaFadeIn(context, MapaPage()));
  }
}
