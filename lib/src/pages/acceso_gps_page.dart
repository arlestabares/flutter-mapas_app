import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class AccesoGpsPage extends StatefulWidget {
  @override
  _AccesoGpsPageState createState() => _AccesoGpsPageState();
}


//Nota: El WidgetsBindingObserver lo que me permite es estar pendiente del cambio
//de estado de la App, cuando se pausa, se pone inactiva, etc. Por ello utilizamos los 
//observer
class _AccesoGpsPageState extends State<AccesoGpsPage>
    with WidgetsBindingObserver {
  //
  @override
  void initState() {
    // this, es para que este pendiente del estado de este widget
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    //this, para que remueva el observador del estado de este Widget cuando ya no lo utilice.
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    //
    print('========> $state');

//Pregunto si el state es igual al resumed.
    if (state == AppLifecycleState.resumed) {
      if (await Permission.location.isGranted) {
        Navigator.pushReplacementNamed(context, 'loading');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Es necesario el Gps para usar esta app'),
            //
            MaterialButton(
                child: Text('Solicitar Acceso',
                    style: TextStyle(color: Colors.white)),
                color: Colors.black,
                shape: StadiumBorder(),
                elevation: 0,
                splashColor: Colors.transparent,
                //
                onPressed: () async {
                  //verificar permisos
                  final status = await Permission.location.request();
                  print(status);
                  //
                  this.accesoAlGps(status);
                })
          ],
        ),
      ),
    );
  }

  void accesoAlGps(PermissionStatus status) {
    print(status);

    switch (status) {
      case PermissionStatus.granted:

        //Nota: Al convertir el Widget a un StateFulWidget por defecto tengo el context
        //accesible sin mas para trabajar con el , como en este caso.
        // Navigator.pushReplacementNamed(context, 'mapa');
        break;

      case PermissionStatus.denied:
      case PermissionStatus.limited:
      case PermissionStatus.restricted:
      case PermissionStatus.undetermined:
      case PermissionStatus.permanentlyDenied:
        openAppSettings();
    }
  }
}
