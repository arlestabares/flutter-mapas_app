import 'package:flutter/material.dart';

import 'package:mapas_app/src/pages/mapa_page.dart';
import 'package:mapas_app/src/pages/loadin_page.dart';
import 'package:mapas_app/src/pages/acceso_gps_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
       home: LoadingPage(),
      //home: AccesoGpsPage(),
      routes: {
        'mapa': (_) => MapaPage(),
        'loading': (_) => LoadingPage(),
        'acceso_gps': (_) => AccesoGpsPage()
      },
    );
  }
}
